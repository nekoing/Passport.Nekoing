<?php
namespace passport;

use app\home\model\AccessToken;
use nekoing\Http;
use nekoing\ErrorCode;

class BaiduOauthLogin implements IOauthLogin
{

    const API_AUTHORIZE = 'http://openapi.baidu.com/oauth/2.0/authorize';

    const API_TOKEN = 'https://openapi.baidu.com/oauth/2.0/token';

    const API_OPENID = 'https://openapi.baidu.com/rest/2.0/passport/users/getLoggedInUser';

    const API_USERINFO = 'https://openapi.baidu.com/rest/2.0/passport/users/getLoggedInUser';
    
    const URL_AVATAR = 'http://tb.himg.baidu.com/sys/portrait/item/';
    
    protected $config;

    public function authorize($scope, $state = '')
    {
        $params = [
            'response_type' => 'code',
            'client_id' => $this->config['appid'],
            'redirect_uri' => $this->config['redirect_uri'],
            'state' => $state,
            'scope' => $this->scopeTransform($scope)
        ];
        
        $url = self::API_AUTHORIZE . '?' . http_build_query($params);
        header('Location: ' . $url);
        exit();
    }

    public function loadConfig($config)
    {
        $this->config = [
            'appid' => $config['appid'],
            'secret' => $config['secret'],
            'redirect_uri' => $config['redirect_uri']
        ];
    }

    /*
     * (non-PHPdoc)
     * @see \passport\IOauthLogin::openid()
     */
    public function openid($token)
    {
        $params = [
            'access_token' => $token->token
        ];
        
        $url = self::API_OPENID . '?' . http_build_query($params);
        
        $res = Http::call($url);
        
        if (! empty($res['uid'])) {
            return $res['uid'];
        } else {
            throw new ExtendOauthException($res['error_description'], $res['error']);
        }
    }

    public function parseAuthorizeCode()
    {
        return input('code');
    }

    public function platform()
    {
        return 'baidu';
    }

    public function refresh($refreshToken)
    {
        // TODO Auto-generated method stub
    }

    public function scopeTransform($scope)
    {
        $scopeMap = [
            AccessToken::SCOPE_BASIC => 'basic'
        ];
        
        $scopeItems = explode(',', $scope);
        $scope = [];
        foreach ($scopeItems as $item) {
            if (isset($scopeMap[$item])) {
                $scope[] = $scopeMap[$item];
            }
        }
        
        return join(' ', array_unique($scope));
    }

    public function token($code)
    {
        $params = [
            'grant_type' => 'authorization_code',
            'client_id' => $this->config['appid'],
            'client_secret' => $this->config['secret'],
            'code' => $code,
            'redirect_uri' => $this->config['redirect_uri']
        ];
        
        $url = self::API_TOKEN . '?' . http_build_query($params);
        
        $res = Http::call($url);
        if (isset($res['access_token'])) {
            $token = new ExtendAccessToken();
            $token->data = $res;
            $token->platform = $this->platform();
            $token->token = $res['access_token'];
            $token->expire = $res['expires_in'];
            $token->refreshToken = $res['refresh_token'];
            $token->appid = $this->config['appid'];
            
            return $token;
        } else {
            throw new ExtendOauthException($res['error'] . ':' . $res['error_description'], ErrorCode::UNKOWN);
        }
    }

    public function userinfo($token)
    {
        $params = [
            'access_token' => $token->token
        ];
        
        $url = self::API_OPENID . '?' . http_build_query($params);
        
        $res = Http::call($url);
        
        if (! empty($res['uid'])) {
            $user = new ExtendUser();
            $user->data = $res;
            $user->platform = $this->platform();
            $user->uid = $res['uid'];
            $user->openid = $res['uid'];
            $user->nickname = $res['uname'];
            $user->avatar = self::URL_AVATAR . $res['portrait'];
            
            return $user;
        } else {
            throw new ExtendOauthException($res['error_description'], $res['error']);
        }
    }
}

?>