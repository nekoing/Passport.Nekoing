<?php
namespace passport;

use passport\IOauthLogin;
use nekoing\Http;
use passport\ExtendAccessToken;
use passport\ExtendOauthException;
use app\home\model\AccessToken;

class WeiboOauthLogin implements IOauthLogin
{

    protected $config;

    const API_AUTHORIZE = 'https://api.weibo.com/oauth2/authorize';

    const API_TOKEN = 'https://api.weibo.com/oauth2/access_token';

    const API_OPENID = 'https://graph.qq.com/oauth2.0/me';

    public function platform()
    {
        return 'weibo';
    }

    public function loadConfig($config)
    {
        $this->config = [
            'appid' => $config['appid'],
            'secret' => $config['secret'],
            'redirect_uri' => $config['redirect_uri']
        ];
    }

    public function authorize($scope, $state = '')
    {
        $params = [
            'response_type' => 'code',
            'client_id' => $this->config['appid'],
            'redirect_uri' => $this->config['redirect_uri'],
            'state' => $state,
            'scope' => $this->scopeTransform($scope)
        ];
        
        $url = self::API_AUTHORIZE . '?' . http_build_query($params);
        header('Location: ' . $url);
        exit();
    }

    public function parseAuthorizeCode()
    {
        return input('code');
    }

    public function refresh($refreshToken)
    {}

    public function scopeTransform($scope)
    {
        $scopeMap = [
            AccessToken::SCOPE_BASIC => 'snsapi_base'
        ];
        
        $scopeItems = explode(',', $scope);
        $scope = [];
        foreach ($scopeItems as $item) {
            if (isset($scopeMap[$item])) {
                $scope[] = $scopeMap[$item];
            }
        }
        
        return join(',', array_unique($scope));
    }

    public function token($code)
    {
        $params = [
            'grant_type' => 'authorization_code',
            'client_id' => $this->config['appid'],
            'client_secret' => $this->config['secret'],
            'code' => $code,
            'redirect_uri' => $this->config['redirect_uri']
        ];
        
        $url = self::API_TOKEN . '?' . http_build_query($params);
        
        $res = Http::call($url, null, true);
        parse_str($res, $res);
        if (! empty($res['access_token'])) {
            $token = new ExtendAccessToken();
            $token->data = $res;
            $token->platform = $this->platform();
            $token->token = $res['access_token'];
            $token->expire = $res['expires_in'];
            $token->appid = $this->config['appid'];
            
            return $token;
        } else {
            throw new ExtendOauthException($res['msg'], $res['code']);
        }
    }

    public function openid($token)
    {
        $params = [
            'access_token' => $token->token
        ];
        
        $url = self::API_OPENID . '?' . http_build_query($params);
        
        $res = Http::call($url, null, true);
        
        if (preg_match('/callback\( \{"client_id":"\w+","openid":"(.*+)"\} \)/', $res, $match)) {
            return $match[1];
        } else {
            parse_str($res, $res);
            throw new ExtendOauthException($res['msg'], $res['code']);
        }
    }

    public function userinfo($token)
    {
        $user = new ExtendUser();
        $user->openid = $token->openid;
        return $user;
    }
}