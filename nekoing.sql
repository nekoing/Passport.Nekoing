/*
Navicat MySQL Data Transfer

Source Server         : vagrant
Source Server Version : 50628
Source Host           : 192.168.222.111:3306
Source Database       : nekoing

Target Server Type    : MYSQL
Target Server Version : 50628
File Encoding         : 65001

Date: 2016-11-16 12:33:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for ps_access_token
-- ----------------------------
DROP TABLE IF EXISTS `ps_access_token`;
CREATE TABLE `ps_access_token` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(128) NOT NULL,
  `scope` varchar(255) NOT NULL,
  `appid` varchar(32) NOT NULL,
  `openid` varchar(32) NOT NULL,
  `token_expire` int(11) NOT NULL,
  `refresh_expire` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  `update_time` int(11) NOT NULL,
  `refresh_token` varchar(32) NOT NULL,
  `refreshed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  KEY `openid` (`openid`,`appid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ps_access_token
-- ----------------------------
INSERT INTO `ps_access_token` VALUES ('22', '1bab29e6fc95a36127deb17d278e143a', 'get_user_info', 'asdasfqwe', 'ae915729d2b59fffea80f2aea3411dae', '7200', '604800', '1479128823', '1479128823', '5e182e83a299ec76d4e1ad009a82da5e', '0');

-- ----------------------------
-- Table structure for ps_auth_code
-- ----------------------------
DROP TABLE IF EXISTS `ps_auth_code`;
CREATE TABLE `ps_auth_code` (
  `code` varchar(32) NOT NULL,
  `scope` varchar(255) NOT NULL,
  `appid` varchar(32) NOT NULL,
  `openid` varchar(32) NOT NULL,
  `expire` int(11) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ps_auth_code
-- ----------------------------

-- ----------------------------
-- Table structure for ps_openid
-- ----------------------------
DROP TABLE IF EXISTS `ps_openid`;
CREATE TABLE `ps_openid` (
  `openid` varchar(255) NOT NULL,
  `appid` varchar(255) NOT NULL,
  `uid` varchar(255) NOT NULL,
  `create_time` int(11) NOT NULL,
  PRIMARY KEY (`openid`),
  UNIQUE KEY `appid` (`appid`,`uid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ps_openid
-- ----------------------------
INSERT INTO `ps_openid` VALUES ('ae915729d2b59fffea80f2aea3411dae', 'asdasfqwe', '5696aa6f06f32cbd', '1479113315');

-- ----------------------------
-- Table structure for ps_third_app
-- ----------------------------
DROP TABLE IF EXISTS `ps_third_app`;
CREATE TABLE `ps_third_app` (
  `id` varchar(32) NOT NULL,
  `name` varchar(32) NOT NULL,
  `secret` varchar(32) NOT NULL,
  `domain` varchar(255) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ps_third_app
-- ----------------------------
INSERT INTO `ps_third_app` VALUES ('asdasfqwe', '哔哩哔哩', 'fasfasf', 'fgyauy.cas.com', '0', '123124521', null);

-- ----------------------------
-- Table structure for ps_user
-- ----------------------------
DROP TABLE IF EXISTS `ps_user`;
CREATE TABLE `ps_user` (
  `id` varchar(32) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `salt` char(5) NOT NULL,
  `nickname` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '0',
  `create_time` int(11) NOT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `nickname` (`nickname`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of ps_user
-- ----------------------------
INSERT INTO `ps_user` VALUES ('5696aa6f06f32cbd', 'nekoing', 'e300a1c1b422ec2c51d6edbb4019e524', '28909', 'nekoing', 'nekoing@live.com', '0', '1478254527', '1478254527');
INSERT INTO `ps_user` VALUES ('8cd934e60418d75f', '1', '2e1a56b386cf98e9ffe9b5e298e71292', '76967', '', '', '0', '1478329684', '1478329684');
