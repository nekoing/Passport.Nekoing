<?php
namespace app\home\validate;

use think\Validate;

class OauthJumpValidate extends Validate
{

    protected $rule = [
        'appid' => 'require|length:6,32|token',
        'redirect_uri' => 'require|url'
    ];
}

?>