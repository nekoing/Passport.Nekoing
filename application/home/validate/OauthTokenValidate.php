<?php
namespace app\home\validate;

use think\Validate;

class OauthTokenValidate extends Validate
{

    protected $rule = [
        'appid' => 'require|length:6,32',
        'grant_type' => 'require',
        'app_secret' => 'require|length:6,32',
        'code' => 'require|length:6,32'
    ];
}

?>