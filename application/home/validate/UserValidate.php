<?php
namespace app\home\validate;

use think\Validate;

class UserValidate extends Validate
{

    protected $rule = [
        'username' => 'require|length:4,32|unique:user',
        'password' => 'require|length:6,16',
        'nickname' => 'require|length:4,32|unique:user',
        'email' => 'require|max:32|unique:user'
    ];
}

?>