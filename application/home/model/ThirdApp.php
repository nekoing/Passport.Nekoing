<?php
namespace app\home\model;

use think\Model;
use nekoing\Utils;

/**
 * @author Nekoing
 *
 * @property string id
 * @property string name
 * @property string secret
 * @property string domain
 * @property integer status
 * @property integer create_time
 * @property integer update_time
 */
class ThirdApp extends Model
{
    protected $auto = ['secret'];
    
    protected function setSecretAttr($value)
    {
        if(! $this->isUpdate) {
            return Utils::uniqid32();
        }
        return $value;
    }
}

?>