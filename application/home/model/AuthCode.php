<?php
namespace app\home\model;

use think\Model;
use nekoing\Utils;

/**
 * @author Nekoing
 *
 * @property string code
 * @property string scope
 * @property string appid
 * @property string openid
 * @property string redirect_uri
 * @property integer expire
 * @property integer create_time
 */
class AuthCode extends Model
{
    
    protected $updateTime = false;
    
    protected $auto = ['code', 'expire'];
    
    protected function setCodeAttr($value)
    {
        if(! $this->isUpdate) {
            return Utils::uniqid32();
        }
        return $value;
    }
    
    protected function setExpireAttr($value)
    {
        return config('oauth.auth_code_life');
    }
}

?>