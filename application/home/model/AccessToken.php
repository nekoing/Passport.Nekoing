<?php
namespace app\home\model;

use think\Model;
use nekoing\Utils;
use nekoing\AppException;

/**
 * @author Nekoing
 *
 * @property integer id
 * @property string token
 * @property string scope
 * @property string appid
 * @property string openid
 * @property integer token_expire
 * @property integer refresh_expire
 * @property integer create_time
 * @property integer update_time
 * @property string refresh_token
 * @property integer refreshed
 */
class AccessToken extends Model
{

    const SCOPE_BASIC = 'basic';

    protected static $scopeInfo = [
        'basic' => '获得您的昵称、头像、性别'
    ];

    public static function availableScopes()
    {
        return [
            self::SCOPE_BASIC
        ];
    }

    public static function scopeInfo()
    {
        return self::$scopeInfo;
    }

    public static function build(AuthCode $authCode)
    {
        self::destroy([
            'openid' => $authCode->openid,
            'appid' => $authCode->appid
        ]);
        
        $row = new self();
        if ($row->save([
            'openid' => $authCode->openid,
            'appid' => $authCode->appid,
            'token' => Utils::uniqid32(),
            'refresh_token' => Utils::uniqid32(),
            'scope' => $authCode->scope,
            'token_expire' => config('oauth.access_token_life'),
            'refresh_expire' => config('oauth.refresh_token_life')
        ])) {
            $authCode->delete();
            return $row;
        }
        
        return false;
    }

    public function getShowData()
    {
        $data = $this->data;
        $data['expire_in'] = $data['token_expire'];
        $data['access_token'] = $data['token'];
        unset($data['create_time']);
        unset($data['update_time']);
        unset($data['appid']);
        unset($data['token']);
        unset($data['token_expire']);
        unset($data['refresh_expire']);
        unset($data['id']);
        
        return $data;
    }

    public function refersh()
    {
        if ($this->refreshed) {
            return false;
        }
        
        if ($token->create_time + $token->token_expire < CURRENT_TIMESTAMP) {
            $this->token_expire = config('oauth.refresh_effect_time');
        } 
        else {
            $this->token_expire = 0;
        }
        $this->refreshed = 1;
        $this->save();
        
        $new = new self();
        $new->data([
            'openid' => $this->openid,
            'appid' => $this->appid,
            'token' => Utils::uniqid32(),
            'refresh_token' => Utils::uniqid32(),
            'scope' => $this->scope,
            'token_expire' => config('oauth.access_token_life'),
            'refresh_expire' => config('oauth.refresh_token_life')
        ]);
        
        $new->save();
        return $new;
    }

        public static function check($token,  $openid = null, $scope = null)
    {
        if (is_string($token) && $token) {
            $token = self::get([
                'token' => $token
            ]);
        }
        
        if(! $token) {
            throw new AppException(100012, 'access_token错误');
        }
        
        if ($openid && $openid != $token->openid) {
            throw new AppException(100012, 'access_token错误');
        }
        
        if ($token->create_time + $token->token_expire < CURRENT_TIMESTAMP) {
            throw new AppException(100013, 'access_token已过期');
        }
        
        if ($scope && ! in_array($scope, explode(',', $token->scope))) {
            throw new AppException(100014, 'access_token权限不足');
        }
        
        return $token;
    }
}

?>