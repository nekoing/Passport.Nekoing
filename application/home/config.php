<?php
return [
    'oauth' => [
        'refresh_effect_time' => 180,
        'auth_code_life' => 600,
        'access_token_life' => 7200,
        'refresh_token_life' => 3600 * 24 * 7, //7天
        'default_scope' => 'basic',
        'extend_oauth_redirect' => 'http://passport.test.nekoing.com/home/jump/'
    ]
];